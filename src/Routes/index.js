import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import UserPage from "../components/UserPage";
import Body from "../components/Body";

export default function Router() {
    return (
        <>
            <Switch>
                <Route exact path="/" component={Body} />
                <Route exact path="/:username" component={UserPage} />
                <Redirect exact from='*' to='/' />
            </Switch>
        </>
    );
}
