import "./App.css";
import Logon from "./components/Logon/Logon";
import React, { useEffect } from "react";
import { useDispatch, connect } from "react-redux";
import {
    saveCurrentUser,
    saveSubscribes,
    loadSubscribes,
    loadUsers,
} from "./store/Users/actions";
import { loggedOn } from "./store/Logon/actions";
import Routes from "./Routes";
import Modal from "./components/Modal";
import {
    saveLikes,
    saveComments,
    loadComments,
    loadLikes,
    loadPosts,

} from "./store/Posts/actions";

function App({
    comments,
    likes,
    subscribes,
    loggOn,
    posts,
    users,
    postsIsloaded,
    usersIsloaded,
    modalIsOpen,
}) {
    const dispatch = useDispatch();
    const storeUser = JSON.parse(localStorage.getItem("currentUser"));
    useEffect(() => {
        if (loggOn) {
            if (!posts.length && !postsIsloaded) {
                    dispatch(loadPosts());
            }
            if (!users.length &&!usersIsloaded) {
                    dispatch(loadUsers());
            }
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [loggOn]);

    if (storeUser) {
        dispatch(saveCurrentUser(storeUser));
        dispatch(loggedOn(true));
    }

    useEffect(() => {
        const subscribes = JSON.parse(localStorage.getItem("subscribes"));
        const likes = JSON.parse(localStorage.getItem("likes"));
        const comments = JSON.parse(localStorage.getItem("comments"));
        if (subscribes) {
            dispatch(saveSubscribes(subscribes));
        } else {
            dispatch(loadSubscribes());
        }
        if (likes) {
            dispatch(saveLikes(likes));
        } else {
            dispatch(loadLikes());
        }
        if (comments) {
            dispatch(saveComments(comments));
        } else {
            dispatch(loadComments());
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        localStorage.setItem("subscribes", JSON.stringify(subscribes));
    }, [subscribes]);
    useEffect(() => {
        localStorage.setItem("likes", JSON.stringify(likes));
    }, [likes]);
    useEffect(() => {
        localStorage.setItem("comments", JSON.stringify(comments));
    }, [comments]);

    return (
        <div className="App">
            {modalIsOpen && <Modal />}
            {!loggOn && <Logon />}
            {loggOn && <Routes />}
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        modalIsOpen: state.modal.isOpen,
        comments: state.postData.comments,
        likes: state.postData.likes,
        subscribes: state.userData.subscribes,
        loggOn: state.login.loggedOn,
        posts: state.postData.posts.postsList,
        users: state.userData.users.usersList,
        postsIsloaded: state.postData.posts.loaded,
        usersIsloaded: state.userData.users.loaded,
    };
};

export default connect(mapStateToProps)(App);
