import axios from "axios";

async function userLogIn(cred) {
    const API = "https://step-inst-parody.herokuapp.com/api";
    const { login, password } = cred;
    const ans = axios(API+"/users").then((res) => {
        const users = res.data.data;
        return users.filter(
            (i) =>
                i.username.toLowerCase() === login.toLowerCase() &&
                i.password === password
        );
    });
    return ans;
}

export { userLogIn };
