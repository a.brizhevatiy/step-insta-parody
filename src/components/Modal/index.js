import React from 'react';
import {useSelector, useDispatch} from "react-redux";
import {setOpenModal} from "../../store/Modal/actions";
import './Modal.scss'

const Modal = () => {
    const dispatch = useDispatch()
    const modalData = useSelector(state => state.modal.data)
    const users = useSelector((state) => state.userData.users.usersList);
    const modalComments = modalData.cmnts.map(comment => {
        const user = users.find(u => u.id === comment.userId)
        return (
            <div className={'modal__comment'} key={comment.userId}>
                <div><span className={'modal__comment--user'}>{user.username}</span>&nbsp;<span
                    className={'modal__comment--email'}>{user.email}:</span>&nbsp;&nbsp;<span
                    className={'modal__comment--text'}>{comment.text}</span></div>
            </div>
        )
    })

    return (
        <>
            <div className="modal__overlay" onClick={(e) => {
                if (e.target.classList.contains("modal__overlay")) {
                    dispatch(setOpenModal(false))
                }
            }}>
                <div className="modal">
                    <div className="modalBody">
                        <img src={modalData.img} alt={""}/>
                    </div>
                    <div className="modalComments">{modalComments}</div>
                </div>
            </div>
        </>
    );
};

export default Modal;
