import React from "react";
import {useSelector, useDispatch} from "react-redux";
import {Link, Redirect} from "react-router-dom";
import './UserPage.scss'
import UserImage from "../UserImage";
import {saveSubscribes} from "../../store/Users/actions";
import {setOpenModal} from "../../store/Modal/actions";
import {setModalData} from "../../store/Modal/actions";
import PropTypes from "prop-types";

export default function UserPage({match}) {
    const dispatch = useDispatch();
    const username = match.params.username;
    const activeUser = useSelector((state) => state.userData.currentUser);
    const users = useSelector((state) => state.userData.users.usersList);
    const posts = useSelector((state) => state.postData.posts.postsList);
    const subscribes = useSelector((state) => state.userData.subscribes);
    const comments = useSelector((state) => state.postData.comments);
    const likes = useSelector((state) => state.postData.likes);

    const user =
        users.find((u) => {
            return u.username === username;
        }) || {};

    if (users.length === 0) {
        return <Redirect to="/"/>;
    }

    const showSubscribeBtn = activeUser.id !== user.id;
    let btnText = subscribes[activeUser.id].indexOf(user.id) !== -1 ? "Unsubscribe" : 'Subscribe';

    const subscribing = () => {
        if (subscribes[activeUser.id].indexOf(user.id) === -1) {
            subscribes[activeUser.id] = [...subscribes[activeUser.id], user.id];
        } else {
            subscribes[activeUser.id] = subscribes[activeUser.id].filter(
                (u) => u !== user.id
            );
        }
        dispatch(saveSubscribes({...subscribes}));
    }

    const showModal = (img, cmnts) => {
        dispatch(setOpenModal(true))
        dispatch(setModalData({img, cmnts}))
    }

    const userPosts = posts
        .filter((post) => post.authorId === user.id)
        .map((i, c) => {
            return (
                <UserImage post={i} key={c} likes={likes[i.postId]} comments={comments[i.postId]}
                           onClick={() => showModal(i.image, comments[i.postId])}/>
            );
        });

    return (
        <div>
            <Link className="user__home--return" to={`/`}>
                <div>
                    <span>-- Return to Main page --</span>
                </div>
            </Link>
            <div className={"user__home"}>
                <div className="user__home--head">
                    <img className={'user__home--photo'} src={user.photo} alt=""/>
                    <h2 className={'user__home--name'}>{user.username}</h2>
                    <p className={'user__home--email'}>{user.email}</p>
                    {showSubscribeBtn && <button className={'user__home--btn'} onClick={subscribing}>{btnText}</button>}
                </div>
            </div>
            <div className="user__home--posts">{userPosts}</div>
        </div>
    );
}

UserPage.propTypes = {
    match: PropTypes.object
}

UserPage.defaultProps = {
    match: {}
}
