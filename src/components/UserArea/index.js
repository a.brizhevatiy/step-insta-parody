import React from "react";
import User from "../User";
import Subscribe from "../Subscribe";
import SubscribeReq from "../SubscribeReq";
import "./UserArea.scss";
import { useDispatch } from "react-redux";
import { loggedOn } from "../../store/Logon/actions";

const UserArea = () => {
    const dispatch = useDispatch();

    function logout() {
        localStorage.removeItem("currentUser");
        localStorage.removeItem("comments")
        localStorage.removeItem("likes")
        localStorage.removeItem("subscribes")
        dispatch(loggedOn(false));
    }

    return (
        <div className={"userZone"}>
            <User />
            <button className="userZone__button--logout" onClick={logout}>
                Logout
            </button>
            <Subscribe />
            <SubscribeReq />
        </div>
    );
};

export default UserArea;