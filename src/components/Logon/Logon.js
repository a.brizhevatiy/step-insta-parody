import React, {useState} from "react";
import {useDispatch} from "react-redux";
import {useForm} from "react-hook-form";
import {loggedOn} from "../../store/Logon/actions";
import "./Logon.scss";
import {userLogIn} from "../../data/functions.js";
import {saveCurrentUser} from "../../store/Users/actions";

const Logon = () => {
    const dispatch = useDispatch();
    const {register, handleSubmit, errors, reset} = useForm();
    const [err, setErr] = useState("");

    async function onSubmit(data) {
        let res = await userLogIn(data);
        if (res.length === 1) {
            dispatch(saveCurrentUser(res[0]));
            dispatch(loggedOn(true));
            localStorage.setItem("currentUser", JSON.stringify(res[0]));
            reset();
            setErr("");
        } else if (res.length === 0) {
            setErr("No such user");
        } else {
            setErr("Something goes wrong");
        }
    }

    return (
        <div className="logon">
            <form onSubmit={handleSubmit(onSubmit)}>
                <div>
                    <p className="logon__login--title">Login</p>
                    <input
                        className="logon__input"
                        type="text"
                        name="login"
                        placeholder="kamren"
                        ref={register({
                            required: "Login can't be empty.",
                        })}
                    />
                    {errors.login && (
                        <p className="logon__login--error">
                            {errors.login.message}
                        </p>
                    )}
                </div>
                <div>
                    <p className="logon__password--title">Password</p>
                    <input
                        className="logon__input"
                        type="password"
                        name="password"
                        placeholder="12345678"
                        ref={register({
                            required: "Password is required.",
                            minLength: {
                                value: 8,
                                message:
                                    "Password should be at-least 8 characters.",
                            },
                        })}
                    />
                    {errors.password && (
                        <p className="logon__password--error">
                            {errors.password.message}
                        </p>
                    )}
                </div>
                <div>
                    <button type="submit" className="logon__submit">
                        Log In
                    </button>
                    <p className="logon__password--error">{err}</p>
                </div>
            </form>
        </div>
    );
}

export default React.memo(Logon)
