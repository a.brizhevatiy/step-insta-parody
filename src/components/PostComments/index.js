import React, {useState} from "react";
import "./PostComments.scss";
import {useSelector} from "react-redux";
import PropTypes from "prop-types";

const PostComments = ({comments, postId, addComment}) => {
    const users = useSelector((state) => state.userData.users.usersList);
    const [allComments, setAllComments] = useState(false);
    const [moreText, setMoreText] = useState("Show all...");
    const [value, setValue] = useState("");
    const [error, setError] = useState(false);
    const errorMessage = "No empty comments, please."

    const showComments = () => {
        if (!allComments) {
            setAllComments(true);
            setMoreText("Hide...");
        } else {
            setAllComments(false);
            setMoreText("Show all...");
        }
    };

    const handleChange = (e) => {
        setValue(e.target.value);
        setError(false);
    };

    const handleSubmit = (e) => {
        if (!value) {
            setError(true);
            e.preventDefault();
            return;
        }
        addComment(value, postId);
        setValue("");
        e.preventDefault();
    };

    if (comments.length === 0) {
        return (
            <>
                <div className="post__comment--head">
                    <form onSubmit={handleSubmit}>
                        <input
                            type="text"
                            className="post__comment--add"
                            placeholder="Add a new comment"
                            name="newComment"
                            value={value}
                            onChange={handleChange}
                        />
                        <input
                            className="post__comment--submit"
                            type="submit"
                            value=">>>"
                        />
                    </form>
                    {error && <p className={"post__comment--error"}>{errorMessage}</p>}
                    Comments:
                </div>
                <div className="post__comment">
                    <span className="post__comment--author">
                        No comments yet.
                    </span>
                </div>
            </>
        );
    }

    const commentsToPost = comments.map((comment, i) => {
        const postUser = users.find((user) => {
            return comment.userId === user.id;
        });

        return (
            <div key={i}>
                <span className="post__comment--author">{postUser.username}:</span>
                &nbsp;
                <span className="post__comment--text">{comment.text}</span>
                {comments.length > 1 && i === 0 && (
                    <div className="post__comment--more" onClick={showComments}>
                        {moreText}
                    </div>
                )}
            </div>
        );
    });

    return (
        <>
            <div className="post__comment--head">
                <form onSubmit={handleSubmit}>
                    <input
                        type="text"
                        className="post__comment--add"
                        placeholder="Add a new comment"
                        name="newComment"
                        value={value}
                        onChange={handleChange}
                    />
                    <input
                        className="post__comment--submit"
                        type="submit"
                        value=">>>"
                    />
                </form>
                {error && <p className={"post__comment--error"}>{errorMessage}</p>}
                Comments:
            </div>
            {!allComments && (
                <div className="post__comment">{commentsToPost[0]}</div>
            )}
            {allComments && (
                <div className="post__comment">{commentsToPost}</div>
            )}
        </>
    );
};

PostComments.propTypes = {
    comments: PropTypes.array,
    postId: PropTypes.number,
    addComment: PropTypes.func,
}

PostComments.defaultProps = {
    comments: [],
    postId: null,
    addComment: () => {},
}

export default React.memo(PostComments);