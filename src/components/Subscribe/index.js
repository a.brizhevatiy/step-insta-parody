import React from "react";
import "./Subscribe.scss";
import {useSelector, useDispatch} from "react-redux";
import {Link} from "react-router-dom";
import {saveSubscribes} from "../../store/Users/actions";


const Subscribe = () => {
    const dispatch = useDispatch();
    const users = useSelector((state) => state.userData.users.usersList);
    const activeUser = useSelector((state) => state.userData.currentUser);
    const subscribes = useSelector((state) => state.userData.subscribes);

    const unsubscribing = (id) => {
        if (subscribes[activeUser.id].indexOf(id) !== -1) {
            subscribes[activeUser.id] = subscribes[activeUser.id].filter(
                (u) => u !== id
            );
        dispatch(saveSubscribes({...subscribes}));
        } else {
            console.error("Something wrong with remove from Sub")
        }
    }

    const userList = users
        .filter((user) => subscribes[activeUser.id].indexOf(user.id) !== -1)
        .map((user, i) => {
            return (
                <div key={i} className={"subscribe__user"}>
                    <div>
                        <Link className={"subscribe__link"} to={`/${user.username}`}>
                            <img className="subscribe__user--photo" src={user.photo} alt={""}/>
                            &nbsp;
                            <span className="subscribe__user--name">
                            {user.username}
                        </span>
                        </Link>
                    </div>
                    <div>

                        <button className={"subscribe__button"} onClick={() => unsubscribing(user.id)}>Unsubscribe
                        </button>
                    </div>
                </div>
            );
        });

    return (
        <div className={"subscribe"}>
            <div className="subscribe__title">You subscribed:</div>
            {userList}
        </div>
    );
};

export default React.memo(Subscribe);