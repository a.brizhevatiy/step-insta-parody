import React from "react";
import "./SubscribeReq.scss";
import {useSelector, useDispatch} from "react-redux";
import {Link} from "react-router-dom";
import {saveSubscribes} from "../../store/Users/actions";

const SubscribeReq = () => {
    const dispatch = useDispatch();
    const users = useSelector((state) => state.userData.users.usersList);
    const activeUser = useSelector((state) => state.userData.currentUser);
    const subscribes = useSelector((state) => state.userData.subscribes);

    const subscribing = (id) => {
        if (subscribes[activeUser.id].indexOf(id) === -1) {
            subscribes[activeUser.id] = [...subscribes[activeUser.id], id];
            dispatch(saveSubscribes({...subscribes}))
        } else {
            console.error("Something wrong with add to Sub")
        }
    }

    const userList = users
        .filter(
            (user) =>
                subscribes[activeUser.id].indexOf(user.id) === -1 &&
                user.id !== activeUser.id
        )
        .map((user, i) => {
            return (
                <div key={i} className={"requests__user"}>
                    <div>
                        <Link className="requests__link" to={`/${user.username}`}>
                            <img className="requests__user--photo" src={user.photo} alt={""}/>
                            &nbsp;
                            <span className="requests__user--name">
                            {user.username}
                        </span>
                        </Link>
                    </div>
                    <div>
                        <button className={"requests__button"} onClick={()=>{subscribing(user.id)}}>Subscribe</button>
                    </div>
                </div>
            );
        });

    return (
        <div className={"requests"}>
            <div className="requests__title">Available:</div>
            {userList}
        </div>
    );
};

export default React.memo(SubscribeReq);