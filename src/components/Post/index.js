import React from "react";
import "./Post.scss";
import Star from "../Icons/Star.jsx";
import PostComments from "../PostComments";
import {useSelector} from "react-redux";
import PropTypes from "prop-types";

const Post = ({
                  pathToImg,
                  authorId,
                  title,
                  comments,
                  toFav,
                  likesLength,
                  inFav,
                  addComment,
                  postId,
                  className
              }) => {

    const users = useSelector((state) => state.userData.users.usersList);
    const user = users.find((u) => u.id === authorId);
    return (
        <div className={`post ${className}` } onDoubleClick={toFav}>
            <div className="post__head">
                <div className="post__title">{title}</div>
                <div className="post__user">
                    <span className="post__user--name">By: {user.username}</span>
                    &nbsp;
                    <span className="post__user--mail">{user.email}</span>
                </div>
            </div>
            <img src={pathToImg} alt="" className={"post__image"}/>
            <PostComments
                postId={postId}
                comments={comments}
                addComment={addComment}
            />
            <Star filled={inFav} count={likesLength}/>
        </div>
    );
};

Post.propTypes={
    postId:PropTypes.number,
    authorId:PropTypes.number,
    likesLength:PropTypes.number,
    title:PropTypes.string,
    className:PropTypes.string,
    pathToImg: PropTypes.string,
    comments:PropTypes.array,
    inFav:PropTypes.bool,
    toFav:PropTypes.func,
    addComment:PropTypes.func,
}
Post.defaultProps={
    postId:null,
    authorId:null,
    likesLength:0,
    title:"",
    className:"",
    pathToImg: "",
    comments:[],
    inFav:false,
    toFav:()=>{},
    addComment:()=>{},
}

export default React.memo(Post);