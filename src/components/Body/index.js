import React from "react";
import UserPosts from "../UserPosts";
import UserArea from "../UserArea";
import "./Body.scss";
import Loader from "../../components/Loader";
import { useSelector } from "react-redux";

export default function Body() {
    const postsIsloaded = useSelector((state) => state.postData.posts.loaded);
    const usersIsloaded = useSelector((state) => state.userData.users.loaded);

    return (
        <div className="body">
            {!postsIsloaded && !usersIsloaded && <Loader />}
            {postsIsloaded && usersIsloaded && <UserPosts />}
            {postsIsloaded && usersIsloaded && <UserArea />}
        </div>
    );
}
