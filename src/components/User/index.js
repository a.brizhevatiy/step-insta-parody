import"./User.scss";
import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

const User = () => {
    const activeUser = useSelector((state) => state.userData.currentUser);
    const { username, email, photo} = activeUser;

    return (
        <div className={"user"}>
            <img src={photo} alt="user" className={"user__photo"} />
            <Link to={`/${username}`}>
                <div className={"user__name"}>{username}</div>
            </Link>
            <div className={"user__email"}>{email}</div>
        </div>
    );
};

export default User;