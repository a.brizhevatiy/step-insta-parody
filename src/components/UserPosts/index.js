import React, { useEffect, useState } from "react";
import "./UserPosts.scss";
import Post from "../Post";
import { useSelector, useDispatch } from "react-redux";
import { saveLikes, saveComments } from "../../store/Posts/actions";

function UserPosts() {
    const [displayPosts, setDisplayPosts] = useState(2);
    const dispatch = useDispatch();
    const posts = useSelector((state) => state.postData.posts.postsList);
    const activeUser = useSelector((state) => state.userData.currentUser);
    const comments = useSelector((state) => state.postData.comments);
    const likes = useSelector((state) => state.postData.likes);
    const subscribes = useSelector((state) => state.userData.subscribes);
    const currentPosts = posts.filter(
        (i) =>
            i.authorId === activeUser.id ||
            subscribes[activeUser.id].indexOf(i.authorId) !== -1
    );

    const favorAdd = (userId, arr, postId) => {
        let likesArr;
        if (arr.indexOf(userId) !== -1) {
            likesArr = arr.filter((l) => l !== userId);
        } else {
            likesArr = [...arr, userId];
        }
        likes[postId] = likesArr;
        dispatch(saveLikes({ ...likes }));
    };

    const addComment = (newComm, postId) => {
        let commArr = comments[postId];
        commArr.unshift({ userId: activeUser.id, text: newComm });
        comments[postId] = commArr;
        dispatch(saveComments({ ...comments }));
    };

    const scrollHandler = (e) => {
        if (
            e.target.documentElement.scrollHeight -
                (e.target.documentElement.scrollTop + window.innerHeight) <
            100
        ) {
            // console.log("qqq->",displayPosts); данный дисплейПост не меняет своего значения и всегда показывает 2.....
            setDisplayPosts((prevState) => prevState + 1);
        }
    };

    useEffect(() => {
        document.addEventListener("scroll", scrollHandler);
        return () => document.removeEventListener("scroll", scrollHandler);
    }, []);

    const postItems = currentPosts
        .filter(
            (i) =>
                i.authorId === activeUser.id ||
                subscribes[activeUser.id].indexOf(i.authorId) !== -1
        )
        .filter((i, count) => {
            // console.log(displayPosts) данный дисплейПост меняет свое значение в отличии от строки 52
            return count <= displayPosts;
        })
        .map((i) => {
            return (
                <Post
                    key={i.postId}
                    postId={i.postId}
                    pathToImg={i.image}
                    toFav={() =>
                        favorAdd(activeUser.id, likes[i.postId], i.postId)
                    }
                    authorId={i.authorId}
                    title={i.title}
                    comments={comments[i.postId]}
                    likesLength={likes[i.postId].length}
                    inFav={likes[i.postId].indexOf(activeUser.id) !== -1}
                    addComment={addComment}
                />
            );
        });

    return <div className="posts">{postItems}</div>;
}

export default React.memo(UserPosts);
