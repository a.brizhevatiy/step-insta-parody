import {combineReducers} from "redux";
import {postsReducer} from './Posts/reducers'
import {usersReducer} from './Users/reducers'
import {loginReducer} from './Logon/reducers'
import {modalReducer} from "./Modal/reducer";

export default combineReducers({
    postData: postsReducer,
    userData: usersReducer,
    login:loginReducer,
    modal: modalReducer
})