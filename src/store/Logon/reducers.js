import {LOGGED_ON} from './actions';

const defaultState = {
    loggedOn: false
}

export const loginReducer = (state = defaultState, action) => {
    switch (action.type) {
        case LOGGED_ON:
            return {...state, loggedOn: action.payload};
        default:
            return state;
    }
}