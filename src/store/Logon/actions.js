export const LOGGED_ON = "LOGGED_ON";


export const loggedOn = (items) => (dispatch) => {
    dispatch({
        type: LOGGED_ON,
        payload: items,
    });
};

