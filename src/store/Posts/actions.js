import axios from "axios";

export const SAVE_POSTS = "SAVE_POSTS";
export const POSTS_LOADED = "POSTS_LOADED";
export const SAVE_COMMENTS = "SAVE_COMMENTS";
export const SAVE_LIKES = "SAVE_LIKES";

const API = "https://step-inst-parody.herokuapp.com/api";

export const savePosts = (items) => (dispatch) => {
    dispatch({
        type: SAVE_POSTS,
        payload: items,
    });
};

export const postsLoaded = (loaded) => (dispatch) => {
    dispatch({
        type: POSTS_LOADED,
        payload: loaded,
    });
};

export const saveComments = (items) => (dispatch) => {
    dispatch({
        type: SAVE_COMMENTS,
        payload: items,
    });
};

export const saveLikes = (items) => (dispatch) => {
    dispatch({
        type: SAVE_LIKES,
        payload: items,
    });
};

export const loadPosts = () => (dispatch) => {
    postsLoaded(false);
    axios(API + "/posts").then((res) => {
        const posts = res.data.data;

        dispatch(savePosts(posts));
        dispatch(postsLoaded(true));
    });
};

export const loadComments = () => (dispatch) => {
    axios(API + "/comments").then((res) => {
        dispatch(saveComments(res.data.data));
    });
};
export const loadLikes = () => (dispatch) => {
    axios(API + "/likes").then((res) => {
        dispatch(saveLikes(res.data.data));
    });
};
