import {
    SAVE_POSTS,
    POSTS_LOADED,
    SAVE_COMMENTS,
    SAVE_LIKES,
} from "./actions";

const defaultState = {
    posts: {
        postsList: [],
        loaded: false,
    },
    comments: {},
    likes: {},
};

export const postsReducer = (state = defaultState, action) => {
    switch (action.type) {
        case SAVE_POSTS:
            return {
                ...state,
                posts: { ...state.posts, postsList: action.payload },
            };
        case POSTS_LOADED:
            return {
                ...state,
                posts: { ...state.posts, loaded: action.payload },
            };
        case SAVE_COMMENTS:
            return {
                ...state,
                comments: action.payload,
            };
        case SAVE_LIKES:
            return {
                ...state,
                likes: action.payload,
            };
        default:
            return state;
    }
};
