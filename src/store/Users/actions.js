import axios from "axios";
export const SAVE_CURRENT_USER = "SAVE_CURRENT_USER";
export const SAVE_USERS = "SAVE_USERS";
export const USERS_LOADED = "USERS_LOADED";
export const SAVE_SUBSCRIBES = "SAVE_SUBSCRIBES";

const API = "https://step-inst-parody.herokuapp.com/api";

export const saveCurrentUser = (items) => (dispatch) => {
    axios(`${API}/subscribes/${items.id}`).then((res) => {
        items.subscribes = res.data.data;
    });
    dispatch({
        type: SAVE_CURRENT_USER,
        payload: items,
    });
};

export const saveUsers = (items) => (dispatch) => {
    dispatch({
        type: SAVE_USERS,
        payload: items,
    });
};

export const usersLoaded = (loaded) => (dispatch) => {
    dispatch({
        type: USERS_LOADED,
        payload: loaded,
    });
};

export const loadUsers = () => (dispatch) => {
    usersLoaded(false);
    axios(API + "/users")
        .then((res) => dispatch(saveUsers(res.data.data)))
        .then(() => dispatch(usersLoaded(true)));
};

export const loadSubscribes = () => (dispatch) => {
    axios(API + "/subscribes").then((res) => {
        dispatch(saveSubscribes(res.data.data));
    });
};

export const saveSubscribes = (items) => (dispatch) => {
    dispatch({
        type: SAVE_SUBSCRIBES,
        payload: items,
    });
};
