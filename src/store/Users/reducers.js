import {
    SAVE_CURRENT_USER,
    SAVE_USERS,
    USERS_LOADED,
    SAVE_SUBSCRIBES,
} from "./actions";

const defaultState = {
    currentUser: {},
    users: {
        usersList: [],
        loaded: false,
    },
    subscribes: {},
};

export const usersReducer = (state = defaultState, action) => {
    switch (action.type) {
        case SAVE_CURRENT_USER:
            return { ...state, currentUser: action.payload };
        case SAVE_USERS:
            return {
                ...state,
                users: { ...state.users, usersList: action.payload },
            };
        case USERS_LOADED:
            return {
                ...state,
                users: { ...state.users, loaded: action.payload },
            };
            case SAVE_SUBSCRIBES:
                return {
                    ...state,
                    subscribes: action.payload,
                };
        default:
            return state;
    }
};
