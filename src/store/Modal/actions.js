export const IS_MODAL_OPEN = "IS_MODAL_OPEN";
export const SET_MODAL_DATA = "SET_MODAL_DATA";

export const setOpenModal = (isOpen) => (dispatch) => {
    dispatch({
        type: IS_MODAL_OPEN,
        payload: isOpen,
    });
};
export const setModalData = (data) => (dispatch) => {
    dispatch({
        type: SET_MODAL_DATA,
        payload: data,
    });
};

