import {
    IS_MODAL_OPEN,
    SET_MODAL_DATA,
} from "./actions";

const defaultState = {
    isOpen: false,
    data: {}
}
export const modalReducer = (state = defaultState, action) => {
    switch (action.type) {
        case IS_MODAL_OPEN:
            return {...state, isOpen: action.payload};
        case SET_MODAL_DATA:
            return {...state, data: action.payload};
        default:
            return state;
    }
};
